provider "aws" {
  region = local.region
}

locals {
  region = var.aws_region # "ap-southeast-2"
}
