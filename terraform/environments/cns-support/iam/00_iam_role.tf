module "iam_role_flowlog" {
  source = "../../../modules/aws/iam/aws_iam_role"

  name = "tf-FlowLog"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "vpc-flow-logs.amazonaws.com"
        }
      },
    ]
  })
  description = "IAM Role for VPC FlowLog"
  inline_policies = [
    {
      name = "policy-FlowLog"
      policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
          {
            Action = [
              "logs:CreateLogGroup",
              "logs:CreateLogStream",
              "logs:PutLogEvents",
              "logs:DescribeLogGroups",
              "logs:DescribeLogStreams",
            ]
            Effect   = "Allow"
            Resource = "*"
          },
        ]
      })
    }
  ]

  tags = {
    Owner       = "Haris Fauzi"
    Environment = "Terraform Testing"
  }
}