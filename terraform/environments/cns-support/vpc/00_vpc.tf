
################################################################################
# VPC Module
################################################################################

module "vpc" {
  source = "../../../modules/aws/vpc/aws_vpc"

  name        = "terraform-mainvpc"
  cidr_block  = "10.228.0.0/20"
  enable_ipv6 = false

  tags = {
    Owner       = "Haris Fauzi"
    Environment = "Terraform Testing"
  }
}
