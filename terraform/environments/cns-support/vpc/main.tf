provider "aws" {
  region = local.region
  assume_role {
    # This is cns-support
    role_arn     = "arn:aws:iam::882744822126:role/cns-rackspace-limited-role"
    session_name = "terraform"
  }
}

provider "aws" {
  alias  = "cns-build-factory-aws"
  region = local.region
  assume_role {
    # This is cns-build-factory-aws
    role_arn     = "arn:aws:iam::897322816589:role/cns-rackspace-admin"
    session_name = "terraform"
  }
}

locals {
  region = var.aws_region # "us-east-2"
}
