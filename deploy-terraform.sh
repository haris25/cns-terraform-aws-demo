#!/bin/bash

set -o pipefail +x

ARG_ARRAY=()

get_short_term_credentials() {
    AWS_ACCESS_KEY=$(grep -A6 "\[${SHORT_AWS_PROFILE}\]" ~/.aws/credentials | grep aws_access_key_id | awk '{print $NF}')
    AWS_SECRET_KEY=$(grep -A6 "\[${SHORT_AWS_PROFILE}\]" ~/.aws/credentials | grep aws_secret_access_key | awk '{print $NF}')
    AWS_SECURITY_TOKEN=$(grep -A6 "\[${SHORT_AWS_PROFILE}\]" ~/.aws/credentials | grep aws_session_token | awk '{print $NF}')
    AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY
    AWS_SECRET_ACCESS_KEY=$AWS_SECRET_KEY
    AWS_SESSION_TOKEN=$AWS_SECURITY_TOKEN
    export AWS_ACCESS_KEY AWS_SECRET_KEY AWS_SECURITY_TOKEN AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN
}

get_long_term_credentials() {
    AWS_ACCESS_KEY=$(grep -A2 "\[${LONG_AWS_PROFILE}\]" ~/.aws/credentials | grep aws_access_key_id | awk '{print $NF}')
    AWS_SECRET_KEY=$(grep -A2 "\[${LONG_AWS_PROFILE}\]" ~/.aws/credentials | grep aws_secret_access_key | awk '{print $NF}')
    AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY
    AWS_SECRET_ACCESS_KEY=$AWS_SECRET_KEY
    export AWS_ACCESS_KEY AWS_SECRET_KEY AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
}

launch() {
    local SCRIPT_ACTION=$1
    local TF_CONFIG=$2
    local DOCKER_IMAGE=hashicorp/terraform
    # local SOURCE_REPO_FULL_URL=$(git remote get-url origin | cut -d':' -f2)
    # local SOURCE_REPO_URL
    # local workspace_name=""
    # if [ "z$(echo ${SOURCE_REPO_FULL_URL} | cut -d':' -f1)" == "zhttps" ]; then
    #   # This is https URL. Need to remove the username password from the URL https://username:password@fqdn/path
    #   SOURCE_REPO_URL="https://$(echo ${SOURCE_REPO_FULL_URL} | cut -d'@' -f2)"
    # else
    #   # This is git ssh URL, pass it as it is to SOURCE_REPO_URL
    #   SOURCE_REPO_URL="${SOURCE_REPO_FULL_URL}"
    # fi
    # local SOURCE_REPO_BRANCH=$(git branch| grep -e '^*' | awk '{print $2}')

    cd "terraform/environments/${TF_CONFIG}"
    if [ "z${SCRIPT_ACTION}" == "zdeploy" ]; then
        ACTION=("apply" "tfplan")
        PLAN_ACTION=("plan")
    elif [ "z${SCRIPT_ACTION}" == "zdestroy" ]; then
        ACTION=("apply" "-destroy")
        PLAN_ACTION=("plan" "-destroy")
    else
      echo "Invalid action. You need to define action as"
      echo "$0 -n <action>"
      echo "Where valid actions are choice of deploy, destroy"
    fi

    TF_PLUGIN_CACHE_DIR="${HOME}/.terraform_cache"
    export TF_PLUGIN_CACHE_DIR
    # Dir check
    [ ! -d "${TF_PLUGIN_CACHE_DIR}" ] && mkdir -p "${TF_PLUGIN_CACHE_DIR}"
    # Switch terraform workspace
    workspace_name=$(echo "${TF_CONFIG}-${AWS_DEFAULT_REGION}" |sed 's,/,-,g')
    # Check if workspace exists
    local workspace_count=$(terraform workspace list | grep "${workspace_name}" | wc -l)
    if [ "${workspace_count}" == "0" ]; then
      echo "Calling"
      echo "terraform workspace new \"${workspace_name}\""
      terraform workspace new "${workspace_name}"
      if [ "$?" != "0" ]; then
        # Error initialising workspace. Let's initialise the backend.
        terraform init -backend-config="../../../backend.tf"
        terraform workspace new "${workspace_name}"
      fi
      terraform workspace select "${workspace_name}"
    else
      echo "Calling"
      echo "terraform workspace select \"${workspace_name}\""
      terraform workspace select "${workspace_name}"
    fi

    # Run terraform init
    echo "Run terraform init"
    terraform init -backend-config="../../../backend.tf" -migrate-state

    # Run terraform plan
    terraform "${PLAN_ACTION[@]}" -input=false -out=tfplan \
      -var-file="../../../variables/main.tfvars" \
      -var aws_region="${AWS_DEFAULT_REGION}" \
      -compact-warnings \
      "${ARG_ARRAY[@]}"
    # Run terraform apply
    if [ "z${DRY_RUN}" == "z" -o "z${DRY_RUN}" == "zfalse" ]; then
      terraform apply tfplan
    fi
    EXIT_STATUS=$?
    cd ../
    exit ${EXIT_STATUS}

}

parse_arguments() {
    while (( "$#" )); do
      case "$1" in
        -l|--long-term-profile)
          LONG_AWS_PROFILE=$2
          # Only a user would have long term profile.
          # User would need to assume the role called user_assumerole
          AWS_ROLE_TO_ASSUME=user_assumerole
          shift 2
          ;;
        -s|--short-term-profile)
          SHORT_AWS_PROFILE=$2
          # SWITCH_ACCOUNT=0
          shift 2
          ;;
        -e|--extra-vars)
          EARG=$2
          shift 2
          ARG_ARRAY+=("-var" $EARG)
          ;;
        -f|--var-file)
          EARG=$2
          shift 2
          ARG_ARRAY+=("-var-file" "/workspace/vars/$EARG")
          ;;
        -n|--action)
          SCRIPT_ACTION=$2
          shift 2
          ;;
        -i|--item)
          TF_CONFIG=$2
          shift 2
          ;;
        -t|--target)
          ARG_ARRAY+=("-target=$2")
          shift 2
          ;;
        -m|--module)
          MODULE=$2
          shift 2
          ;;
        -d|--dry-run)
          DRY_RUN=$2
          shift 2
          ;;
        --) # end argument parsing
          shift
          break
          ;;
        -*|--*=) # unsupported flags
          echo "Error: Unsupported flag $1" >&2
          exit 1
          ;;
        *) # preserve positional arguments
          PARAMS="$PARAMS $1"
          shift
          ;;
      esac
    done
    # set positional arguments in their proper place
    eval set -- "$PARAMS"
}

main() {

    # SWITCH_ACCOUNT=1
    parse_arguments $@

    if [ "z$SHORT_AWS_PROFILE" != "z" ]; then
        get_short_term_credentials
    elif [ "z$LONG_AWS_PROFILE" != "z" ]; then
        get_long_term_credentials
    fi
    launch "${SCRIPT_ACTION}" "${TF_CONFIG}"

}

main "$@"
