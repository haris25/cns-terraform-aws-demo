# README

This repo demonstrates the separation of AWS IAM resources implementation and the rest of AWS resources.
This repo is produced for Rackspace team to ensure they can write code to deploy IAM resources which will be deployed by Canstar team, whilst they can continue their work with other resources and deploy these other resources by themselves.

## Requirement

- terraform > 1.2.6
- bash (preferrably running on Linux instead of WSL)

## TL;DR;

### Deploying IAM Resources

1. This is meant to be performed by Canstar team only.
2. Authenticate to either `cns-support` or `cns-dc` using `saml2aws` or other tool to gain CLI access.
3. Set the usual environment variables for AWS authentication, to connect to `cns-support`:
    ```shell
    AWS_PROFILE=cns-support
    AWS_DEFAULT_REGION=ap-southeast-2
    export AWS_PROFILE AWS_DEFAULT_REGION
    ```
4. Deploy the IAM resources:
    ```shell
    ./deploy-terraform.sh --short-term-profile ${AWS_PROFILE} --item cns-support/iam --action deploy
    ```
5. Or, to destroy the IAM resources:
    ```shell
    ./deploy-terraform.sh --short-term-profile ${AWS_PROFILE} --item cns-support/iam --action destroy
    ```

### Deploying VPC Resources (in this demo)
1. Authenticate to `cns-build-factory` AWS account using `cns-rackspace-role` IAM role.
2. Set the following environment to point to the AWS profile you generate
    with `saml2aws`, and the AWS region you want to use for deployment:
    ```shell
    AWS_PROFILE=cns-rackspace-role
    AWS_DEFAULT_REGION=ap-southeast-1
    export AWS_PROFILE AWS_DEFAULT_REGION
    ```
3. Deploy the VPC resources:
    ```shell
    ./deploy-terraform.sh --short-term-profile ${AWS_PROFILE} --item cns-support/vpc --action deploy
    ```
4. Or, to destroy the VPC resources:
    ```shell
    ./deploy-terraform.sh --short-term-profile ${AWS_PROFILE} --item cns-support/vpc --action destroy
    ```

### Expanding the code

1. Create directort under `terraform/environments/` that represents the AWS account for target deployment.
2. To put more IAM resources, place your code under `.../iam` directory. Canstar will deploy them.
3. To deploy other resources, place your code outside `.../iam` but under the correct account (`cns-support`/`cns-dc`). Rackspace team should have the correct privilege to deploy them.
4. Ensure you point to the correct location for the `--item` flag.
5. Update the backend to point to the correct S3+DynamoDB location with proper permission.

Copyright (C) Haris Fauzi <haris.fauzi@versent.com.au>
